#!/bin/bash
docker run -it \
--env SECURE_LOG_LEVEL=debug --env CI_PROJECT_DIR=/tmp/project \
--volume $PWD:/tmp/project \
registry.gitlab.com/gitlab-org/security-products/analyzers/spotbugs:2 \
/analyzer analyze /tmp/project
